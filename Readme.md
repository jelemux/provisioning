# provisioning

My vision of how a provisioning tool should work.  
This is a work-in-progress.  

I don't know when I'm gonna implement this.  

I initially thought of implementing this in Rust, but I'm not sure anymore if that's a good idea.  
Zig or maybe Drew DeVault's new language may also be possible options.  

## Goals

### Design
- Configuration in TOML files (This is not clear yet) - no programming necessary
- Main runtime reads files and executes specified commands.
- Three (or maybe four) kinds of files:
    - Playbooks:
        - Provide the runtime with instructions on what to do.
        - Should be unambiguous and expressive.
        - Can be used with some kind of templating system - Jinja2,Tera?
    - Modules:
        - They provide the bindings for the commands that can be used in playbooks.
        - Prefferably use the same file format as playbooks.
        - Modules may also define informational functions which can be used in templates.
    - Interfaces:
        - Interfaces that can be implemented by modules to make them more interchangeable.
        - The idea is here to make it possible to for example declare a default package manager and use that through the playbook.
        - Probably defined as a JSON schema.
    - Plugins (far future):
        - Extend the main runtime with additional features.
        - Can be used like modules.
        - Maybe WebAssembly?
- Cross-platform-capabilities: I'm not completely sure yet but I think I don't like modules being system-agnostic. Instead, it should be defined in the playbooks, what modules to use for which system. The system can be determined in the temlplate.
- Dependency management: There should be an official index, but otherwise I think it's best to keep dependencies decentralized. Since dependency trees probably won't be very big, dependencies can be just referenced by URL. But I'm not sure yet if that's the best approach.

### Shortterm
- Main runtime
- Local playbook execution only
- Only command-line and maybe file system API - everything else can be done through that at first
- Interfaces and Modules for common functionality that wrap around the command line, possibly:
    - file and directory management
    - user & group management
    - package management
    - init systems
- Example implementations of those modules (maybe Alpine as first target?)

### Midterm
- Templating system
- Implement more APIs besides command-line
- Vagrant plugin

### Longterm
- Remote execution
- Plugins